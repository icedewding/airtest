# -*- coding: utf-8 -*-
# @Time    : 2022/2/27 16:50
# @Author  : icedew
# @File    : runner.py
# @Software: PyCharm
import unittest

from BeautifulReport import BeautifulReport

suite = unittest.TestSuite()
TestCases = unittest.TestLoader().loadTestsFromModule("case文件")
suite.addTest(TestCases)
runner = BeautifulReport(suite)
# runner = unittest.TextTestRunner()
runner.report("测试报告","report.html")