# -*- coding: utf-8 -*-
# @Time    : 2022/2/27 16:31
# @Author  : icedew
# @File    : basesettings.py
# @Software: PyCharm
import os

basedir = os.path.dirname(os.path.dirname(__file__))
logdir = os.path.join(basedir,"logs")
reportdir = os.path.join(basedir,"report")
picdir = os.path.join(basedir,"testdata","images")

if __name__ == '__main__':
    print(picdir)